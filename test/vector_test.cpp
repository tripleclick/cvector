#include <gtest/gtest.h>
#include <vector>
#include <tuple>

extern "C" {
#include <vector.h>
}

struct Foo {
    int x;
    int y;
    friend bool operator==(const Foo & lhs, const Foo & rhs);
};

inline bool operator==(const Foo & lhs, const Foo & rhs)
{
    return std::tie(lhs.x, lhs.y) == std::tie(rhs.x, rhs.y);
}

template <typename T>
class TestVector : public::testing::Test {

public:
    void SetUp() override {
        vector_init(&v, sizeof(T), &TestVector<T>::copy_function);
    }

    void TearDown() override {
        vector_free(&v);
    }

    static void copy_function(void *dest_item, const void *src_item) {
        memcpy(dest_item, src_item, sizeof(T));

    }

    Vector v;
};

using VectorOfInts = TestVector<int>;
using VectorOfStructs = TestVector<Foo>;

TEST_F(VectorOfInts, SizeOfEmptyVectorShouldBeZero)
{
    const int emptySize = 0;

    const int actualSize = vector_get_size(&v);

    EXPECT_EQ(emptySize, actualSize);
}

TEST_F(VectorOfInts, SizeShouldIncreaseAfterAddingItems)
{
    const int expectedSize1 = 1;
    const int expectedSize2 = 2;
    int testValue = 432;

    vector_add(&v, &testValue);
    int actualSize = vector_get_size(&v);
    EXPECT_EQ(expectedSize1, actualSize);

    vector_add(&v, &testValue);
    actualSize = vector_get_size(&v);
    EXPECT_EQ(expectedSize2, actualSize);
}

TEST_F(VectorOfInts, SizeShouldDecreaseAfterDeletingItems)
{
    const int emptySize = 0;
    const int expectedSize1 = 1;
    const int expectedSize2 = 2;
    int testValue = 523;

    vector_add(&v, &testValue);
    vector_add(&v, &testValue);

    int actualSize = vector_get_size(&v);
    EXPECT_EQ(expectedSize2, actualSize);

    vector_delete(&v, 0);
    actualSize = vector_get_size(&v);
    EXPECT_EQ(expectedSize1, actualSize);

    vector_delete(&v, 0);
    actualSize = vector_get_size(&v);
    EXPECT_EQ(emptySize, actualSize);
}

TEST_F(VectorOfInts, GettingInvalidIndexShouldReturnNull)
{
    int testValue = 13;
    const int *expectedValue = NULL;

    vector_add(&v, &testValue);
    const int *actualValue = static_cast<int*>(vector_get(&v, 1));
    EXPECT_EQ(expectedValue, actualValue);
}

TEST_F(VectorOfInts, AddedItemShouldHaveSameValue)
{
    int testValue = 13;
    const int expectedValue = testValue;

    vector_add(&v, &testValue);
    int *actualValue = static_cast<int*>(vector_get(&v, 0));
    EXPECT_EQ(expectedValue, *actualValue);
}

TEST_F(VectorOfInts, AddingItemShouldCopyTheOriginalItem)
{
    int testValue = 543;
    const int expectedValue = testValue;

    vector_add(&v, &testValue);

    int *actualValue = static_cast<int*>(vector_get(&v, 0));
    EXPECT_NE(&expectedValue, actualValue);
}

TEST_F(VectorOfInts, GettingItemShouldNotCopyTheItem)
{
    int testValue = 75;
    vector_add(&v, &testValue);

    int *actualValue = static_cast<int*>(vector_get(&v, 0));
    int *actualValue2 = static_cast<int*>(vector_get(&v, 0));
    EXPECT_EQ(actualValue, actualValue2);
}

TEST_F(VectorOfInts, SettingItemShouldNotCopyTheItem)
{
    int testValue = 75;
    int testValue2 = 145;
    const int expectedValue = 145;
    vector_add(&v, &testValue);

    int *actualValue = static_cast<int*>(vector_get(&v, 0));
    vector_set(&v, 0, &testValue2);
    int *actualValue2 = static_cast<int*>(vector_get(&v, 0));

    EXPECT_EQ(actualValue, actualValue2);
    EXPECT_EQ(expectedValue, *actualValue2);
}

TEST_F(VectorOfInts, AddingAndGettingItemsShouldNotChangeTheOrder)
{
    std::vector<int> testValues {1, 12, 23, 14, 8};
    const std::vector<int> expectedValues(testValues);

    for (std::size_t i = 0; i < testValues.size(); ++i) {
        vector_add(&v, &testValues[i]);
    }

    for (std::size_t i = 0; i < expectedValues.size(); ++i) {
        int *actualValue = static_cast<int*>(vector_get(&v, i));
        EXPECT_EQ(expectedValues[i], *actualValue);
    }
}

TEST_F(VectorOfInts, DeleteFirstItemShouldNotChangeTheOthers)
{
    std::vector < int >testValues {1, 12, 23, 14, 8};
    const std::vector < int >expectedValues {12, 23, 14, 8};

    for (std::size_t i = 0; i < testValues.size(); ++i) {
        vector_add(&v, &testValues[i]);
    }

    vector_delete(&v, 0);

    for (std::size_t i = 0; i < expectedValues.size(); ++i) {
        int *actualValue = static_cast<int*>(vector_get(&v, i));
        EXPECT_EQ(expectedValues[i], *actualValue);
    }
}

TEST_F(VectorOfInts, DeleteLastItemShouldNotChangeTheOthers)
{
    std::vector<int> testValues {1, 12, 23, 14, 8};
    const std::vector<int> expectedValues {1, 12, 23, 14};

    for (std::size_t i = 0; i < testValues.size(); ++i) {
        vector_add(&v, &testValues[i]);
    }

    vector_delete(&v, 4);

    for (std::size_t i = 0; i < expectedValues.size() - 1; ++i) {
        int *actualValue = static_cast<int*>(vector_get(&v, i));
        EXPECT_EQ(expectedValues[i], *actualValue);
    }
}

TEST_F(VectorOfInts, DeleteMiddleItemShouldNotChangeTheOthers)
{
    std::vector<int> testValues {1, 12, 23, 14, 8};
    const std::vector<int> expectedValues {1, 12, 14, 8};

    for (std::size_t i = 0; i < testValues.size(); ++i) {
        vector_add(&v, &testValues[i]);
    }

    vector_delete(&v, 2);

    for (std::size_t i = 0; i < expectedValues.size(); ++i) {
        int *actualValue = static_cast<int*>(vector_get(&v, i));
        EXPECT_EQ(expectedValues[i], *actualValue);
    }
}

TEST_F(VectorOfInts, DeletingInvalidIndexShouldBeIgnored)
{
    int testValue = 12;
    const int expectedSize = 1;

    vector_add(&v, &testValue);

    vector_delete(&v, 1);

    const int actualSize = vector_get_size(&v);

    EXPECT_EQ(expectedSize, actualSize);

}

TEST_F(VectorOfInts, VectorMemoryBlockShouldBeContinuous)
{
    std::vector<int> testValues {1, 12, 23, 14, 8};

    for (std::size_t i = 0; i < testValues.size(); ++i) {
        vector_add(&v, &testValues[i]);
    }

    const int *firstItem = static_cast<int*>(vector_get(&v, 0));

    for (std::size_t i = 0; i < testValues.size(); ++i) {
        int *actualValue = static_cast<int*>(vector_get(&v, i));
        EXPECT_EQ(actualValue, firstItem + i);
    }
}

TEST_F(VectorOfInts, ExistingItemInVectorShouldBeFound)
{
    std::vector<int> testValues {1, 12, 23, 14, 8};
    int expectedValue = 23;
    const int expectedPosition = 2;

    for (std::size_t i = 0; i < testValues.size(); ++i) {
        vector_add(&v, &testValues[i]);
    }

    auto compare_function =[](const void *lhs, const void *rhs){
        const int a = *(static_cast<const int*>(lhs));
        const int b = *(static_cast<const int*>(rhs));

        return (a < b) ? -1 : (a == b ? 0 : 1);
    };

    const int index = vector_find(&v, &expectedValue, compare_function);

    EXPECT_EQ(expectedPosition, index);
}

TEST_F(VectorOfInts, FindingNonExistingElementShouldReturnInvalidIndex)
{
    int testValue = 23;
    const int searchValue = 44;
    const int expectedPosition = -1;

    vector_add(&v, &testValue);

    auto compare_function =[](const void *lhs, const void *rhs){
        const int a = *(static_cast<const int*>(lhs));
        const int b = *(static_cast<const int*>(rhs));

        return (a < b) ? -1 : (a == b ? 0 : 1);
    };

    const int index = vector_find(&v, &searchValue, compare_function);

    EXPECT_EQ(expectedPosition, index);
}

TEST_F(VectorOfStructs, VectorMemoryBlockShouldBeContinuous)
{
    std::vector<Foo> testValues {{1, 2}, {4, 2}, {31, 2}, {14, 2}, {16, 2}};
    const std::vector<Foo> expectedValues {{1, 2}, {4, 2}, {31, 2}, {14, 2}, {16, 2}};

    for (std::size_t i = 0; i < testValues.size(); ++i) {
        vector_add(&v, &testValues[i]);
    }

    Foo *firstItem = static_cast<Foo*>(vector_get(&v, 0));

    for (std::size_t i = 0; i < testValues.size(); ++i) {
        Foo *actualValue = static_cast<Foo*>(vector_get(&v, i));
        EXPECT_EQ(actualValue, firstItem + i);
        EXPECT_EQ(*actualValue, *(firstItem + i));
        EXPECT_EQ(expectedValues[i], *actualValue);
    }
}

TEST_F(VectorOfStructs, DeleteMiddleItemShouldNotChangeTheOthers)
{
    std::vector<Foo> testValues {{1, 2}, {4, 2}, {31, 2}, {14, 2}, {16, 2}};
    const std::vector<Foo> expectedValues {{1, 2}, {4, 2}, {14, 2}, {16, 2}};

    for (std::size_t i = 0; i < testValues.size(); ++i) {
        vector_add(&v, &testValues[i]);
    }

    vector_delete(&v, 2);

    for (std::size_t i = 0; i < expectedValues.size(); ++i) {
        Foo *actualValue = static_cast<Foo*>(vector_get(&v, i));
        EXPECT_EQ(expectedValues[i], *actualValue);
    }
}

