#ifndef VECTOR_H
#define VECTOR_H

#define VECTOR_INIT_CAPACITY 4

/**
 * A structure representing vector data structure.
 */
typedef struct Vector {
    void *items;                //!< Array of items.
    int capacity;               //!< Allocated capacity of the vector
    int size;                   //!< Number of elements in the vector.
    int item_size;              //!< Size of a single element in the vector.
    void (*copy_item) (void *destination, const void *source);  //!< Function copying single element.
} Vector;

/**
 * @brief Function to init the vector. It should be called before any usage of vector.
 * @param[in,out] v Pointer to vector.
 * @param[in] item_size Size of stored item in bytes.
 * @param[in] copy_function Function copying the item from one place in memory to another.
 */
void vector_init(Vector * v, const int item_size, void (*copy_function) (void *destination, const void *source));

/**
 * @brief Function to init the vector. It has to be called before any usage of vector.
 * @param[in,out] v Pointer to vector. It has to be already allocated.
 * @param[in] item_size Size of stored item in bytes.
 * @param[in] copy_function Function copying the item from one place in memory to another.
 */
void vector_free(Vector *);

/**
 * @brief Function to add element to the vector.
 * @param v Pointer to vector. It has to be already initiated.
 * @param item Element which will be added to the vector.
 * @return Returns the new size of the vector, or -1 if adding failed.
 */
int vector_add(Vector * v, const void *item);

/**
 * @brief Function to delete the element from the vector.
 * @param v Pointer to vector. It has to be already initiated.
 * @param index Index of element in the vector to be deleted.
 */
void vector_delete(Vector * v, const int index);

/**
 * @brief Function to find the element in the vector.
 * @param v Pointer to vector. It has to be already initiated.
 * @param item Pointer to the element to be found.
 * @param compare_function Function comparing two elements.
 *      It's return values should be consistent with memcmp and strcmp.
 * @return Index of found element ot -1 if not found.
 */
int vector_find(Vector * v, const void *item, int (*compare_function) (const void *, const void *));

/**
 * @brief Function to get the element from the vector.
 * @param v Pointer to vector. It has to be already initiated.
 * @param index Index of element in the vector to be obtained.
 * @return Pointer to found element. NULL if not found due to invalid index parameter.
 */
void *vector_get(Vector * v, const int index);

/**
 * @brief  Function to set the element in the vector.
 * @param v Pointer to vector. It has to be already initiated.
 * @param index Index of element in the vector to be overwritten.
 * @param item Pointer to the new item.
 */
void vector_set(Vector * v, int index, void *item);

/**
 * @brief Function to get the current size of vector.
 * @param v Pointer to vector. It has to be already initiated.
 * @return The size of the vector.
 */
int vector_get_size(Vector * v);

#endif                          // VECTOR_H
