# cvector

Vector implementation in C

[![pipeline status](https://gitlab.com/tripleclick/cvector/badges/master/pipeline.svg)](https://gitlab.com/tripleclick/cvector/commits/master)

[![coverage report](https://gitlab.com/tripleclick/cvector/badges/master/coverage.svg)](https://tripleclick.gitlab.io/cvector/)