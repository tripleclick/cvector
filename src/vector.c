#include <stdlib.h>
#include <string.h>

#include "vector.h"

void vector_init(Vector *v, const int item_size, void (*copy_function) (void *destination, const void *source))
{
    v->capacity = VECTOR_INIT_CAPACITY;
    v->size = 0;
    v->item_size = item_size;
    v->copy_item = copy_function;

    int data_size = v->item_size *v->capacity;
    v->items = malloc(data_size);
    if (v->items == NULL) {
        v->capacity = 0;
        v->size = -1;
        return;
    }
    memset(v->items, 0, data_size);
}

void vector_free(Vector *v)
{
    free(v->items);
    v->copy_item = NULL;
    v->items = NULL;
}

static int vector_resize(Vector *v, const int capacity)
{
    void *items = realloc(v->items, v->item_size * capacity);
    if (items) {
        v->items = items;
        v->capacity = capacity;
        return v->capacity;
    }
    return -1;
}

int vector_add(Vector *v, const void *item)
{
    if (v->capacity == v->size) {
        if (vector_resize(v, v->capacity * 2) == -1) {
            return -1;
        };
    }

    void *new_item = v->items + (v->size++ * v->item_size);
    v->copy_item(new_item, item);

    return v->size;
}

void vector_delete(Vector *v, const int index)
{
    if (index < 0 || index >= v->size) {
        return;
    }

    memmove(v->items + (index * v->item_size),
            v->items + ((index + 1) * v->item_size), 
            (v->size - index - 1) * v->item_size);

    v->size--;
}

void* vector_get(Vector *v, const int index)
{
    if (index >= 0 && index < v->size) {
        return v->items + (index * v->item_size);
    }

    return NULL;
}

void vector_set(Vector *v, const int index, void *item)
{
    if (index >= 0 && index < v->size) {
        v->copy_item(v->items + (index * v->item_size), item);
    }
}

int vector_get_size(Vector * v)
{
    return v->size;
}

int vector_find(Vector * v, const void *item, int (*compare_function) (const void *, const void *))
{
    for (int i = 0; i < v->size; ++i) {
        if (compare_function(v->items + (i * v->item_size), item) == 0) {
            return i;
        }
    }
    return -1;
}

