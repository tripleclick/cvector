#include <gtest/gtest.h>
#include <vector>
#include <tuple>

extern "C" {
#include <vector.h>
#include <memhelper.h>
} 

class AllocFailTest:public::testing::Test {
 public:

    void SetUp() override {
    }

    void TearDown() override {
        vector_free(&v);
    }

    static void copy_function(void *dest_item, const void *src_item) {
        memcpy(dest_item, src_item, sizeof(int));
    }

    Vector v;
};

TEST_F(AllocFailTest, VectorInitFailedOnMalloc)
{
    const int invalidSize = -1;

    malloc_counter = 0;
    malloc_trigger = 1;

    vector_init(&v, sizeof(int), &AllocFailTest::copy_function);

    const int actualSize = vector_get_size(&v);

    EXPECT_EQ(invalidSize, actualSize);
}

TEST_F(AllocFailTest, VectorFailOnReallocShouldReturnLastGoodSize)
{
		const int expectedSize = -1;
    std::vector<int> testValues {1, 12, 23, 14, 8};
    const std::vector<int> expectedValues {1, 12, 23, 14};

    realloc_counter = 0;
    realloc_trigger = 1;

    vector_init(&v, sizeof(int), &AllocFailTest::copy_function);

    for (std::size_t i = 0; i < testValues.size() - 1; ++i) {
        const int size = vector_add(&v, &testValues[i]);
        EXPECT_EQ((i + 1), size);
    }

    int actualSize = vector_add(&v, &testValues[4]);

    EXPECT_EQ(expectedSize, actualSize);

    for (std::size_t i = 0; i < expectedValues.size(); ++i) {
        int *actualValue = static_cast<int*>(vector_get(&v, i));
        EXPECT_EQ(expectedValues[i], *actualValue);
    }
}

