#define _GNU_SOURCE
#include "memhelper.h"
#include <stdio.h>
#include <dlfcn.h>

int malloc_counter = 0;
int malloc_trigger = -1;
int realloc_counter = 0;
int realloc_trigger = -1;

static void *(*real_malloc) (size_t) = NULL;
static void *(*real_realloc) (void *, size_t) = NULL;

void *malloc(size_t size)
{
    if (++malloc_counter == malloc_trigger) {
        return NULL;
    }

    if (!real_malloc) {
        real_malloc = dlsym(RTLD_NEXT, "malloc");
    }

    return real_malloc(size);
}

void *realloc(void *ptr, size_t size)
{
    if (++realloc_counter == realloc_trigger) {
        return NULL;
    }

    if (!real_realloc) {
        real_realloc = dlsym(RTLD_NEXT, "realloc");
    }

    return real_realloc(ptr, size);
}

