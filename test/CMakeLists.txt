cmake_minimum_required(VERSION 3.5)
project(cvector-test)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup(NO_OUTPUT_DIRS)

add_definitions("-Wall -std=c++11")

add_executable(vector-test vector_test.cpp)
target_link_libraries(vector-test
		cvector
    ${CONAN_LIBS}
    "--coverage"
)

add_executable(vector-allocfail-test alloc_fail_test.cpp)
target_link_libraries(vector-allocfail-test
		memhelper
		cvector
		${CONAN_LIBS}
		"--coverage"
)

set_target_properties(vector-test vector-allocfail-test
    PROPERTIES
    RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin
)

enable_testing()

add_test(NAME vector-test COMMAND
         WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/bin
         COMMAND vector-test "--gtest_output=xml:${CMAKE_BINARY_DIR}/unittest_report1.xml")

add_test(NAME vector-allocfail-test
  			 WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/bin
				 COMMAND vector-allocfail-test "--gtest_output=xml:${CMAKE_BINARY_DIR}/unittest_report2.xml")

